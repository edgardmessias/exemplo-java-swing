/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.exemplo.java.swing.componentes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author Edgard Lorraine Messias <edgard.messias@romera.com.br>
 */
public class CidadeJComboBox extends JComboBox<String> {

    protected DefaultComboBoxModel<String> model;

    protected EstadoJComboBox estadoComboBox;

    protected ActionListener listener;

    public CidadeJComboBox() {
        this.model = new DefaultComboBoxModel<>();

        this.setModel(this.model);

        this.listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                atualizarCidades();
            }
        };
    }

    protected boolean atualizarCidades() {
        this.model.removeAllElements();

        if (this.estadoComboBox == null) {
            return false;
        }

        String estado = this.estadoComboBox.getEstadoSelecionado();
        if (estado == null) {
            return false;
        }

        estado = "Alguma cidade do " + estado;

        this.model.addElement(estado);
        this.model.setSelectedItem(estado);

        return true;
    }

    public EstadoJComboBox getEstadoComboBox() {
        return estadoComboBox;
    }

    public void setEstadoComboBox(EstadoJComboBox estadoComboBox) {
        if (this.estadoComboBox != null) {
            this.estadoComboBox.removeActionListener(this.listener);
        }

        this.estadoComboBox = estadoComboBox;
        this.estadoComboBox.addActionListener(listener);
        this.atualizarCidades();
    }

}
