/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.exemplo.java.swing.componentes;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author Edgard Lorraine Messias <edgard.messias@romera.com.br>
 */
public class EstadoJComboBox extends JComboBox<String> {

    protected List<String> estados;

    public EstadoJComboBox() {

        this.estados = new ArrayList<>();

        this.estados.add("Acre");
        this.estados.add("Alagoas");
        this.estados.add("Amapá");
        this.estados.add("Amazonas");
        this.estados.add("Bahia");
        this.estados.add("Ceará");
        this.estados.add("Distrito Federal");
        this.estados.add("Espírito Santo");
        this.estados.add("Goiás");
        this.estados.add("Maranhão");
        this.estados.add("Mato Grosso do Sul");
        this.estados.add("Mato Grosso");
        this.estados.add("Minas Gerais");
        this.estados.add("Paraná");
        this.estados.add("Paraíba");
        this.estados.add("Pará");
        this.estados.add("Pernambuco");
        this.estados.add("Piauí");
        this.estados.add("Rio Grande do Norte");
        this.estados.add("Rio Grande do Sul");
        this.estados.add("Rio de Janeiro");
        this.estados.add("Rondônia");
        this.estados.add("Roraima");
        this.estados.add("Santa Catarina");
        this.estados.add("Sergipe");
        this.estados.add("São Paulo");
        this.estados.add("Tocantins");

        // .toArray(new String[this.estados.size()]) -> truque para converter para array tipado
        this.setModel(new DefaultComboBoxModel<>(this.estados.toArray(new String[this.estados.size()])));
    }

    public String getEstadoSelecionado() {
        int index = this.getSelectedIndex();

        try {
            return this.estados.get(index);
        } catch (Exception e) {
        }

        return null;
    }

}
